package it.mbettiol.dynamicjackson;

import java.io.IOException;
import java.util.Map;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class JacksonTests {

	@Test
	public void serialize() throws JsonProcessingException, JSONException{
		FirstDTO firstDTO = new FirstDTO();
		firstDTO.setName("hello");
		ObjectMapper objectMapper = new ObjectMapper();
		String writeValueAsString = objectMapper.writeValueAsString(firstDTO);
		String expected = "{\"name\":\"hello\"}";
		JSONAssert.assertEquals(expected, writeValueAsString, false);
	}
	
	@Test
	public void deserialize() throws JSONException, IOException{
		FirstDTO firstDTO = new FirstDTO();
		String source = "{\"name\":\"hello\"}";
		firstDTO.setName("hello");
		ObjectMapper objectMapper = new ObjectMapper();
		FirstDTO readValue = objectMapper.readValue(source, FirstDTO.class);
		Assert.assertEquals("hello", readValue.getName());
	}
	
	@Test
	public void unknownAreKept() throws JSONException, IOException{
		ObjectMapper objectMapper = new ObjectMapper();
		String received = "{\"name\":\"hello\", \"testunknown\":1}";
		FirstDTO readValue = objectMapper.readValue(received, FirstDTO.class);
		String sent = objectMapper.writeValueAsString(readValue);
		JSONAssert.assertEquals(received, sent, true);
	}
	
	
	@Test
	public void dynamicSetAndGetTest(){
		FirstDTO firstDTO = new FirstDTO();
		firstDTO.setPrezzo(100L);
		Assert.assertEquals((Long)100L,firstDTO.getPrezzo());
		
	}
}
