package it.mbettiol.dynamicjackson;

public class FirstDTO extends DynamicDTO{

	private static final String NAME = "name";
	
	private static Key<Long> _prezzo; //avoid conflict
	
	public String getName(){
		return (String)getData().get(NAME);
	}
	
	public void setName(String name){
		getData().put(NAME,name);
	}
	
	public Long getPrezzo(){
		return getValue(_prezzo);
	}
	
	public void setPrezzo(Long prezzo){
		setValue(_prezzo,prezzo);
	}
	
}
