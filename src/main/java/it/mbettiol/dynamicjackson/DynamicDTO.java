package it.mbettiol.dynamicjackson;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.deser.ResolvableDeserializer;

/**
 * Marker Interface
 * @author marcobettiol
 *
 */

@JsonDeserialize(using = DynamicDTO.DynDeserializer.class)
@JsonSerialize(using = DynamicDTO.DynSerializer.class)
public abstract class DynamicDTO {

	private static Map<Class, Properties> properties = new ConcurrentHashMap<>();
	
	public DynamicDTO(){
		Class<?> lookupClass = getClass();
		Properties p = properties.get(lookupClass);
		if(p==null){
			ConcreteDynamicInitializer<Object> concreteDynamicInitializer = new ConcreteDynamicInitializer<>();
			Properties initialize = concreteDynamicInitializer.initialize(lookupClass);
			properties.put(lookupClass, initialize);
		}
		//TODO handle caching
	}
//	public static abstract class DyanmicKey<T> {
//		
//		private Class<T> clazz;
//		private String name;
//		public DyanmicKey(String name){
//			this.name = name;
//			 clazz =  (Class<T>) ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments()[0];
//		}
//		
//		public T get(Map<String,Object> values){
//			return clazz.cast(values.get(name));
//		}
//	}
	
	private static class Properties{
		
		Map<String,Class<?>> props;
	}
	
	public static final class Key<T>{
		private String name;
		
		private Key(String name){ 
			this.name = name;
		}
	};
	
	private Map<String, Object> data = new HashMap<>();
	
	
		protected Map<String, Object> getData() {
			return data;
		}
		
		protected <T> T getValue(Key<T> key) {
			return (T) data.get(key.name);
		}
		
		<T> void  setValue(Key<T> key, T value) {
		  data.put(key.name,value);
		}

		//
		// Serializza e deserializza come semplice stringa
		//
		public static class DynSerializer extends JsonSerializer<DynamicDTO> {

			@Override
			public void serialize(DynamicDTO dto, JsonGenerator generator, SerializerProvider provider)
					throws IOException, JsonProcessingException {
				generator.writeObject(dto.data);
			}
		}
		
		public static class DynDeserializer extends JsonDeserializer<DynamicDTO> implements ResolvableDeserializer, ContextualDeserializer {
		    
			JavaType contextualType;
			
			public DynDeserializer(){
				
			}
			
			


			public DynDeserializer(JavaType contextualType) {
				super();
				this.contextualType = contextualType;
			}




			@Override
		    public DynamicDTO deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
		        ObjectCodec oc = jsonParser.getCodec();
		        Class<?> rawClass = contextualType.getRawClass();
		        try {
		        	DynamicDTO newInstance = (DynamicDTO) rawClass.newInstance();
					Map readValue = oc.readValue(jsonParser, Map.class);
					newInstance.data = readValue;
					return (DynamicDTO) newInstance;//TODO
				} catch (InstantiationException | IllegalAccessException e) {
					throw new RuntimeException(e);
				}
		     //   String readValue = oc.readValue(jsonParser, String.class);
		    }

			@Override
			public void resolve(DeserializationContext ctxt) throws JsonMappingException {
				JavaType contextualType = ctxt.getContextualType();
				
			}

			@Override
			public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property)
					throws JsonMappingException {
				JavaType contextualType = ctxt.getContextualType();
				return new DynDeserializer(contextualType);
			}
			
		    @Override
		    public boolean isCachable() { return true; }
		}
		
		public static class ConcreteDynamicInitializer<T>{
			
			public Properties initialize(Class<?> concreteDynamic){
				Map<String,Class<?>> result = new HashMap<>();
				Field[] declaredFields = concreteDynamic.getDeclaredFields();
				for(Field f : declaredFields){
					if(Modifier.isStatic(f.getModifiers())){
						String name = f.getName();
						Class<?> type = f.getType();
						if(Key.class.isAssignableFrom(type)){
							//TOD extract right type
							result.put(name.substring(1, name.length()), type);
							f.setAccessible(true);
							try {
//								f.setInt(f, f.getModifiers() & ~Modifier.FINAL);
								f.set(null, new Key<>(f.getName()));
							} catch (IllegalArgumentException| IllegalAccessException e) {
								throw new RuntimeException(e);
							}
						}
						
						
						//TODO istantiate static fields
					}
				}
				Properties properties2 = new Properties();
				properties2.props = result;
				//TODO check getter and setters are aligned
				return properties2;
				
			}
			
		}
}


